// homework_12.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void Print()
{
    std::cout << "Hellow, Skillbox!\n";
}

int PrintInt(int ToPrint)
{
    std::cout << ToPrint << "\n";

    return ToPrint;
}

long long Sum(long long a, long long b)
{
    return a + b;
}

int main()
{
    Print();

    std::cout << PrintInt(10);

    std::cout << "\n" << Sum(2231212342, 34990488);

    double TestDouble = 10.1;
    bool TestBool = true;

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
